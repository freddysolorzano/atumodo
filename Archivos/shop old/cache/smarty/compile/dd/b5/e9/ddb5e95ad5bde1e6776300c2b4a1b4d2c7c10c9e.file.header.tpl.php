<?php /* Smarty version Smarty-3.1.13, created on 2013-05-22 12:34:29
         compiled from "/Users/freddysolorzano/Sites/sexshop/modules/addshoppers/header.tpl" */ ?>
<?php /*%%SmartyHeaderCode:155772759519cfa9d8d0383-96936919%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ddb5e95ad5bde1e6776300c2b4a1b4d2c7c10c9e' => 
    array (
      0 => '/Users/freddysolorzano/Sites/sexshop/modules/addshoppers/header.tpl',
      1 => 1368629954,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '155772759519cfa9d8d0383-96936919',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'module_dir' => 0,
    'meta_title' => 0,
    'is_product_page' => 0,
    'image_url' => 0,
    'logo_url' => 0,
    'shop_name' => 0,
    'meta_description' => 0,
    'product_name' => 0,
    'product_description' => 0,
    'price' => 0,
    'stock' => 0,
    'instock' => 0,
    'shop_id' => 0,
    'floating_buttons' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_519cfa9d9bf3b4_22277848',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_519cfa9d9bf3b4_22277848')) {function content_519cfa9d9bf3b4_22277848($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_escape')) include '/Users/freddysolorzano/Sites/sexshop/tools/smarty/plugins/modifier.escape.php';
if (!is_callable('smarty_modifier_replace')) include '/Users/freddysolorzano/Sites/sexshop/tools/smarty/plugins/modifier.replace.php';
?>

<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['module_dir']->value;?>
static/css/shop.css" />
<meta property="og:title" content="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['meta_title']->value, 'htmlall', 'UTF-8');?>
" />
<?php if ($_smarty_tpl->tpl_vars['is_product_page']->value){?>
<meta property="og:type" content="addshoppers:product" />
<?php if (isset($_smarty_tpl->tpl_vars['image_url']->value)){?>
<meta property="og:image" content="<?php echo $_smarty_tpl->tpl_vars['image_url']->value;?>
" />
<?php }?>
<?php }else{ ?>
<meta property="og:type" content="addshoppers:website" />
<meta property="og:image" content="<?php echo $_smarty_tpl->tpl_vars['logo_url']->value;?>
" />
<?php }?>
<meta property="og:site_name" content="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['shop_name']->value, 'htmlall', 'UTF-8');?>
" />
<meta property="og:description" content="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['meta_description']->value, 'html', 'UTF-8');?>
" />


<!-- AddShoppers.com Sharing Script -->
<script type="text/javascript">
// <![CDATA[

  var AddShoppersTracking = {
  <?php if ($_smarty_tpl->tpl_vars['is_product_page']->value){?>
      name: "<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['product_name']->value, 'htmlall', 'UTF-8');?>
",
      description: "<?php echo smarty_modifier_replace(smarty_modifier_replace(smarty_modifier_escape($_smarty_tpl->tpl_vars['product_description']->value, 'html', 'UTF-8'),"\r\n",''),"\n",'');?>
",
      image: "<?php if (isset($_smarty_tpl->tpl_vars['image_url']->value)){?><?php echo $_smarty_tpl->tpl_vars['image_url']->value;?>
<?php }else{ ?><?php echo $_smarty_tpl->tpl_vars['logo_url']->value;?>
<?php }?>",
      price: "<?php echo $_smarty_tpl->tpl_vars['price']->value;?>
",
      stock: "<?php echo $_smarty_tpl->tpl_vars['stock']->value;?>
"
      <?php if (isset($_smarty_tpl->tpl_vars['instock']->value)){?>,instock: <?php echo $_smarty_tpl->tpl_vars['instock']->value;?>
<?php }?>
  <?php }else{ ?>
      name: '<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['meta_title']->value, 'htmlall', 'UTF-8');?>
',
      description: '<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['meta_description']->value, 'html', 'UTF-8');?>
',
      image: '<?php if (isset($_smarty_tpl->tpl_vars['image_url']->value)){?><?php echo $_smarty_tpl->tpl_vars['image_url']->value;?>
<?php }else{ ?><?php echo $_smarty_tpl->tpl_vars['logo_url']->value;?>
<?php }?>'
  <?php }?>
  };

  var js = document.createElement('script'); js.type = 'text/javascript'; js.async = true; js.id = 'AddShoppers';
  js.src = ('https:' == document.location.protocol ? 'https://shop.pe/widget/' : 'http://cdn.shop.pe/widget/') + 'widget_async.js#<?php echo $_smarty_tpl->tpl_vars['shop_id']->value;?>
';
  document.getElementsByTagName("head")[0].appendChild(js);
// ]]>
</script>


<!-- AddShoppers.com Buttons Script -->
<?php if ($_smarty_tpl->tpl_vars['floating_buttons']->value){?>
<div class="share-buttons share-buttons-tab" data-buttons="twitter,facebook,email,pinterest" data-style="medium" data-counter="true" data-hover="true" data-promo-callout="true" data-float="left"></div>
<?php }?>


<script type="text/javascript">
  jQuery(document).ready(function() {
    var header = $("#header");
    if (header.length > 0)
      header.after($("#addshoppers_buttons"));

    var fb = $("#left_share_fb");
    if (fb.length > 0)
      fb.hide();
  });
</script>

<?php }} ?>