<?php /* Smarty version Smarty-3.1.13, created on 2013-05-15 10:29:23
         compiled from "/Users/freddysolorzano/Sites/sexshop/modules/addshoppers/content.tpl" */ ?>
<?php /*%%SmartyHeaderCode:17039873955193a2cb4e24a5-27130994%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'dafa2b97aa7544334745e4ea7b1f51fd883e3a98' => 
    array (
      0 => '/Users/freddysolorzano/Sites/sexshop/modules/addshoppers/content.tpl',
      1 => 1368629954,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '17039873955193a2cb4e24a5-27130994',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'account_is_configured' => 0,
    'output' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_5193a2cb5aee14_44796218',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5193a2cb5aee14_44796218')) {function content_5193a2cb5aee14_44796218($_smarty_tpl) {?>

<div id="fb-root"></div>
<script type="text/javascript">(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!--[if lt IE 9]>
<style>
	.top .wrap .adshoppers{
		width: 99.5%;
	}
	.top .wrap .box-holder .box{
		width: 32.65%;
	}
	.top .wrap .adshoppers{
		font-size: 22px;
	}
	.top .wrap, .bottom .wrap{
		min-width: 1000px;
	}
</style>
<![endif]-->
<div class="top">
        <div class="logo">
                <img src="../modules/addshoppers/static/img/logo.png">
        </div>
        <div class="wrap">
                <div class="adshoppers">
                        <span class="orange"><?php echo smartyTranslate(array('s'=>'FREE:','mod'=>'addshoppers'),$_smarty_tpl);?>
</span> 
                        <?php echo smartyTranslate(array('s'=>'Addshoppers adds social sharing buttons to your site that help your products get shared more,<br />along with detailed analytics that reveal the ROI of social sharing.','mod'=>'addshoppers'),$_smarty_tpl);?>
<br />
                        <a href="#addshoppers-setup" class="candy" style="text-decoration:underline"><?php echo smartyTranslate(array('s'=>'Get started','mod'=>'addshoppers'),$_smarty_tpl);?>
</a> <?php echo smartyTranslate(array('s'=>'or','mod'=>'addshoppers'),$_smarty_tpl);?>
 <a href="http://www.addshoppers.com/prestashop" target="_blank" class="candy" style="text-decoration:underline"><?php echo smartyTranslate(array('s'=>'learn more','mod'=>'addshoppers'),$_smarty_tpl);?>
</a>.
                </div>
                <div class="box-holder">
                        <div class="box hand">
                                <div class="inner">
                                        <h2><?php echo smartyTranslate(array('s'=>'Sharing Buttons','mod'=>'addshoppers'),$_smarty_tpl);?>
</h2>
                                        <div class="text">
                                                <?php echo smartyTranslate(array('s'=>'Ideal for sharing products, not just articles and content.','mod'=>'addshoppers'),$_smarty_tpl);?>

                                                <ul>
                                                  <li><?php echo smartyTranslate(array('s'=>'Facebook Want and Own buttons','mod'=>'addshoppers'),$_smarty_tpl);?>
</li>
                                                  <li><?php echo smartyTranslate(array('s'=>'See your most Pinned products','mod'=>'addshoppers'),$_smarty_tpl);?>
</li>
                                                </ul>
                                        </div>
                                </div>
                        </div>
                        <div class="box dollar">
                                <div class="inner">
                                        <h2><?php echo smartyTranslate(array('s'=>'Social Rewards','mod'=>'addshoppers'),$_smarty_tpl);?>
</h2>
                                        <div class="text">
                                                <?php echo smartyTranslate(array('s'=>'Increase customer engagement and lower shopping cart abandonment.','mod'=>'addshoppers'),$_smarty_tpl);?>

                                                <ul>
                                                  <li><?php echo smartyTranslate(array('s'=>'Increase sharing by over 2x','mod'=>'addshoppers'),$_smarty_tpl);?>
</li>
                                                  <li><?php echo smartyTranslate(array('s'=>'Set different rewards and tips per source','mod'=>'addshoppers'),$_smarty_tpl);?>
</li>
                                                </ul>
                                        </div>
                                </div>
                        </div>
                        <div class="box navigation">
                                <div class="inner">
                                        <h2><?php echo smartyTranslate(array('s'=>'Social Analytics','mod'=>'addshoppers'),$_smarty_tpl);?>
</h2>
                                        <div class="text">
                                                <?php echo smartyTranslate(array('s'=>'Measure the value of social to orders and revenue.','mod'=>'addshoppers'),$_smarty_tpl);?>

                                                <ul>
                                                  <li><?php echo smartyTranslate(array('s'=>'Stop guessing about social media ROI','mod'=>'addshoppers'),$_smarty_tpl);?>
</li>
                                                  <li><?php echo smartyTranslate(array('s'=>'See which orders were socially influenced','mod'=>'addshoppers'),$_smarty_tpl);?>
</li>
                                                </ul>
                                        </div>
                                </div>
                        </div>
                        <div class="clear"></div>
                </div>
                <div class="adshoppers-free">
                        <?php if ($_smarty_tpl->tpl_vars['account_is_configured']->value){?>
                            <span style="color:green"><?php echo smartyTranslate(array('s'=>'Account is active.','mod'=>'addshoppers'),$_smarty_tpl);?>
</span> You're tracking stats! <a href="http://addshoppers.com" target="_blank">View your stats</a>.
                        <?php }else{ ?>
                            <b><span class="dark candy"><?php echo smartyTranslate(array('s'=>'Addshoppers is completely <em>free</em>','mod'=>'addshoppers'),$_smarty_tpl);?>
</span></b> - <a href="#addshoppers-setup" class="candy" style="text-decoration:underline"><?php echo smartyTranslate(array('s'=>'get started','mod'=>'addshoppers'),$_smarty_tpl);?>
</a> <?php echo smartyTranslate(array('s'=>'by creating your account below.','mod'=>'addshoppers'),$_smarty_tpl);?>

                        <?php }?>
                </div>
        </div>
</div>
<div class="bottom">
        <div class="wrap">
                <div class="left-nav">
                        <?php echo $_smarty_tpl->tpl_vars['output']->value;?>

                </div>
                <div class="right-nav">
                        <div class="feeds">
                                <img src="../modules/addshoppers/static/img/feeds.png">
                        </div>
                        <div class="big-black">
                                <?php echo smartyTranslate(array('s'=>'100\'s of button styles available to match your site\'s look & feel. Place social buttons anywhere.','mod'=>'addshoppers'),$_smarty_tpl);?>

                        </div>
                        <div class="lear-more">
                                <a href="http://help.addshoppers.com/knowledgebase/articles/98896-social-sharing-button-placement-examples" target="_blank"><?php echo smartyTranslate(array('s'=>'learn more','mod'=>'addshoppers'),$_smarty_tpl);?>
</a>
                        </div>

                        <div class="need-help">
                                <h2><?php echo smartyTranslate(array('s'=>'Need help?','mod'=>'addshoppers'),$_smarty_tpl);?>
</h2>
                                <span class="url"><a href="http://forums.addshoppers.com" target="_blank">eCommerce Forums</a></span>
                        </div>

                        <div class="about">
                          <h2><?php echo smartyTranslate(array('s'=>'Advanced integration instruction','mod'=>'addshoppers'),$_smarty_tpl);?>
</h2>
                          <p><?php echo smartyTranslate(array('s'=>'To change button types or positioning on any theme:','mod'=>'addshoppers'),$_smarty_tpl);?>
</p>

                          <ol>
                            <li>1. <?php echo smartyTranslate(array('s'=>'Login to your','mod'=>'addshoppers'),$_smarty_tpl);?>
 <a href="https://www.addshoppers.com/merchants" target="_blank">AddShoppers Merchant Admin</a>.</li>
                            <li>2. <?php echo smartyTranslate(array('s'=>'From the left navigation, go to','mod'=>'addshoppers'),$_smarty_tpl);?>
 <i>Get Apps -> Sharing Buttons</i></li>
                            <li>3. <?php echo smartyTranslate(array('s'=>'Select the button you want and copy the div code.','mod'=>'addshoppers'),$_smarty_tpl);?>
</li>
                            <li>4. <?php echo smartyTranslate(array('s'=>'Find file <i>product.tpl</i> in <i>themes/prestashop</i>.','mod'=>'addshoppers'),$_smarty_tpl);?>
</li>
                            <li>5. <?php echo smartyTranslate(array('s'=>'Paste our code where you want the buttons to appear.','mod'=>'addshoppers'),$_smarty_tpl);?>
</li>
                            <li>6. <?php echo smartyTranslate(array('s'=>'Don\'t forget to create canonical links for products or install the appropriate PrestaShop module.','mod'=>'addshoppers'),$_smarty_tpl);?>
</li>
                          </ol>
                        </div>

                        <div class="about">
                                <h2><?php echo smartyTranslate(array('s'=>'About AddShoppers','mod'=>'addshoppers'),$_smarty_tpl);?>
</h2>
                                <?php echo smartyTranslate(array('s'=>'AddShoppers is a free social sharing and analytics platform built for eCommerce.
                                We make it easy to add social sharing buttons to your site, measure the ROI of social at
                                the SKU level, and increase sharing by rewarding social actions. You\'ll discover the value
                                of social sharing, identify influencers, and decrease shopping cart abandonment by adding
                                AddShoppers social apps to your store.','mod'=>'addshoppers'),$_smarty_tpl);?>


                                <div class="get-started">
                                        <a href="http://www.addshoppers.com" target="_blank"><?php echo smartyTranslate(array('s'=>'Get started with your free account at AddShoppers.com.','mod'=>'addshoppers'),$_smarty_tpl);?>
</a>
                                </div>
                        </div>
                        <div>
                                <?php echo smartyTranslate(array('s'=>'If you\'re a large enterprise retailer who needs a more custom solution, <a href="http://www.addshoppers.com/enterprise" target="_blank">learn more</a>.','mod'=>'addshoppers'),$_smarty_tpl);?>

                        </div>
                </div>
                <div class="clear"></div>
<div style="margin-top: 25px; text-align: center;">
<?php echo smartyTranslate(array('s'=>'By installing the AddShoppers module you agree to our ','mod'=>'addshoppers'),$_smarty_tpl);?>

<a href="http://www.addshoppers.com/terms" target="_blank" title="<?php echo smartyTranslate(array('s'=>'Terms','mod'=>'addshoppers'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'Terms','mod'=>'addshoppers'),$_smarty_tpl);?>
</a>
 <?php echo smartyTranslate(array('s'=>'and','mod'=>'addshoppers'),$_smarty_tpl);?>
 <a href="http://www.addshoppers.com/privacy" target="_blank" title="<?php echo smartyTranslate(array('s'=>'Privacy Policy','mod'=>'addshoppers'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'Privacy Policy','mod'=>'addshoppers'),$_smarty_tpl);?>
</a>
</div>
        </div>
</div>
<?php }} ?>