<?php /* Smarty version Smarty-3.1.13, created on 2013-06-11 15:47:45
         compiled from "/Users/freddysolorzano/Sites/sexshop/modules/mercadopago/confirm.tpl" */ ?>
<?php /*%%SmartyHeaderCode:205368957551b785e96d32b8-80718936%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'baab9c294fa57cf70164f983223be120fdb5edd0' => 
    array (
      0 => '/Users/freddysolorzano/Sites/sexshop/modules/mercadopago/confirm.tpl',
      1 => 1366114470,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '205368957551b785e96d32b8-80718936',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'imgBtn' => 0,
    'this_path_ssl' => 0,
    'currencies' => 0,
    'currency' => 0,
    'total' => 0,
    'imgBnr' => 0,
    'base_dir_ssl' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_51b785e97907e1_27437088',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_51b785e97907e1_27437088')) {function content_51b785e97907e1_27437088($_smarty_tpl) {?><?php $_smarty_tpl->_capture_stack[0][] = array('path', null, null); ob_start(); ?><?php echo smartyTranslate(array('s'=>'Shipping'),$_smarty_tpl);?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./breadcrumb.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<h2><?php echo smartyTranslate(array('s'=>'Order summary','mod'=>'mercadopago'),$_smarty_tpl);?>
</h2>

<?php $_smarty_tpl->tpl_vars['current_step'] = new Smarty_variable('payment', null, 0);?>
<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./order-steps.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<h3><?php echo smartyTranslate(array('s'=>'Pagamento via MercadoPago','mod'=>'mercadopago'),$_smarty_tpl);?>
</h3>


<p>
	<img src="<?php echo $_smarty_tpl->tpl_vars['imgBtn']->value;?>
" alt="<?php echo smartyTranslate(array('s'=>'mercadopago','mod'=>'mercadopago'),$_smarty_tpl);?>
" style="float:left; margin: 0px 10px 5px 0px;" />
	<br /><br />
	<br /><br />
</p>

<form action="<?php echo $_smarty_tpl->tpl_vars['this_path_ssl']->value;?>
validation.php" method="post">
<p style="margin-top:20px;">
	<?php echo smartyTranslate(array('s'=>'Valor total do pedido:','mod'=>'mercadopago'),$_smarty_tpl);?>

	<?php if (count($_smarty_tpl->tpl_vars['currencies']->value)>1){?>
		<?php  $_smarty_tpl->tpl_vars['currency'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['currency']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['currencies']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['currency']->key => $_smarty_tpl->tpl_vars['currency']->value){
$_smarty_tpl->tpl_vars['currency']->_loop = true;
?>
			<span id="amount_<?php echo $_smarty_tpl->tpl_vars['currency']->value['id_currency'];?>
" class="price" style="display:none;">R$ <?php echo $_smarty_tpl->tpl_vars['total']->value;?>
</span>
		<?php } ?>
	<?php }else{ ?>
			<span id="amount_<?php echo $_smarty_tpl->tpl_vars['currencies']->value[0]['id_currency'];?>
" class="price">R$ <?php echo $_smarty_tpl->tpl_vars['total']->value;?>
</span>
	<?php }?>
</p>
<p>
	<b><?php echo smartyTranslate(array('s'=>'Por favor confira as formas de pagamento aceitas pelo MercadoPago e 
	confirme sua compra clicando em \'Confirmar Compra\'','mod'=>'mercadopago'),$_smarty_tpl);?>
.</b>
</p>

<p>
	<center><img src="<?php echo $_smarty_tpl->tpl_vars['imgBnr']->value;?>
" alt="<?php echo smartyTranslate(array('s'=>'Formas de Pagamento MercadoPago','mod'=>'mercadopago'),$_smarty_tpl);?>
"></center>
</p>

<p class="cart_navigation">
	<a href="<?php echo $_smarty_tpl->tpl_vars['base_dir_ssl']->value;?>
order.php?step=3" class="button_large"><?php echo smartyTranslate(array('s'=>'Outras formas de pagamento','mod'=>'mercadopago'),$_smarty_tpl);?>
</a>
	<input type="submit" name="submit" value="<?php echo smartyTranslate(array('s'=>'Confirmar Compra','mod'=>'mercadopago'),$_smarty_tpl);?>
" class="exclusive_large" />
</p>
</form><?php }} ?>