<?php /* Smarty version Smarty-3.1.13, created on 2013-05-15 10:29:23
         compiled from "/Users/freddysolorzano/Sites/sexshop/modules/addshoppers/registrationForm.tpl" */ ?>
<?php /*%%SmartyHeaderCode:13728098075193a2cb432f54-04051993%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '23a139971e73b910cdd7793d2f371a39fb2faf43' => 
    array (
      0 => '/Users/freddysolorzano/Sites/sexshop/modules/addshoppers/registrationForm.tpl',
      1 => 1368629954,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '13728098075193a2cb432f54-04051993',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'action' => 0,
    'module_dir' => 0,
    'email' => 0,
    'password' => 0,
    'confirmPassword' => 0,
    'categories' => 0,
    'item' => 0,
    'category' => 0,
    'phone' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_5193a2cb4b1d22_32725349',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5193a2cb4b1d22_32725349')) {function content_5193a2cb4b1d22_32725349($_smarty_tpl) {?>
<form action="<?php echo $_smarty_tpl->tpl_vars['action']->value;?>
" method="post">
        <fieldset class="width2" id="addshoppers-setup">
                <legend><img src="<?php echo $_smarty_tpl->tpl_vars['module_dir']->value;?>
../../img/admin/cog.gif" alt="" class="middle" /><?php echo smartyTranslate(array('s'=>'Create New Account','mod'=>'addshoppers'),$_smarty_tpl);?>
</legend>

                <label><?php echo smartyTranslate(array('s'=>'Email Address','mod'=>'addshoppers'),$_smarty_tpl);?>
</label>
                <div class="margin-form">
                        <input type="text" name="addshoppers_email" value="<?php echo $_smarty_tpl->tpl_vars['email']->value;?>
" />
                </div>

                <label><?php echo smartyTranslate(array('s'=>'Password','mod'=>'addshoppers'),$_smarty_tpl);?>
</label>
                <div class="margin-form">
                        <input type="password" name="addshoppers_password" value="<?php echo $_smarty_tpl->tpl_vars['password']->value;?>
" />
                </div>

                <label><?php echo smartyTranslate(array('s'=>'Confirm Password','mod'=>'addshoppers'),$_smarty_tpl);?>
</label>
                <div class="margin-form">
                        <input type="password" name="addshoppers_confirm_password" value="<?php echo $_smarty_tpl->tpl_vars['confirmPassword']->value;?>
" />
                </div>

                <label><?php echo smartyTranslate(array('s'=>'Category','mod'=>'addshoppers'),$_smarty_tpl);?>
</label>
                <div class="margin-form">
                        <select name="addshoppers_category" id="addshoppers_category">
                        <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['categories']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
                            <?php if ($_smarty_tpl->tpl_vars['item']->value['value']==$_smarty_tpl->tpl_vars['category']->value){?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['item']->value['value'];?>
" selected="selected"><?php echo $_smarty_tpl->tpl_vars['item']->value['label'];?>
</option>
                            <?php }else{ ?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['item']->value['value'];?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value['label'];?>
</option>
                            <?php }?>
                        <?php } ?>
                        </select>
                </div>

                <label><?php echo smartyTranslate(array('s'=>'Phone (optional)','mod'=>'addshoppers'),$_smarty_tpl);?>
</label>
                <div class="margin-form">
                        <input type="text" name="addshoppers_phone" value="<?php echo $_smarty_tpl->tpl_vars['phone']->value;?>
" />
                </div>

                <center><input type="submit" name="addshoppers_registration" value="<?php echo smartyTranslate(array('s'=>'Create Free Account','mod'=>'addshoppers'),$_smarty_tpl);?>
" class="button" /></center>
        </fieldset>
</form>
<?php }} ?>